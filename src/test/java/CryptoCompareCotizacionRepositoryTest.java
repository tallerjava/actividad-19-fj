
import com.cotizacionweb.repository.CryptoCompareCotizacionRepository;
import jodd.http.HttpException;
import org.json.JSONException;
import org.junit.Test;
import static org.junit.Assert.*;

public class CryptoCompareCotizacionRepositoryTest {

    public CryptoCompareCotizacionRepositoryTest() {
    }

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_sinUrl_httpException() {
        CryptoCompareCotizacionRepository cryptoCompareCotizacionRepository = new CryptoCompareCotizacionRepository();
        cryptoCompareCotizacionRepository.setUrl("");
        cryptoCompareCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = JSONException.class)
    public void obtenerCotizacion_urlInvalida_JSONException() {
        CryptoCompareCotizacionRepository cryptoCompareCotizacionRepository = new CryptoCompareCotizacionRepository();
        cryptoCompareCotizacionRepository.setUrl("https://api.coindesk.com/v1/bpi/currentprice.xml");
        cryptoCompareCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = NullPointerException.class)
    public void obtenerCotizacion_urlNull_NullPointerException() {
        CryptoCompareCotizacionRepository cryptoCompareCotizacionRepository = new CryptoCompareCotizacionRepository();
        cryptoCompareCotizacionRepository.setUrl(null);
        cryptoCompareCotizacionRepository.obtenerCotizacion();
    }

    @Test
    public void obtenerCotizacion_urlValida_cotizacionObtenida() {
        CryptoCompareCotizacionRepository cryptoCompareCotizacionRepository = new CryptoCompareCotizacionRepository();
        assertNotNull(cryptoCompareCotizacionRepository.obtenerCotizacion());
    }
}
