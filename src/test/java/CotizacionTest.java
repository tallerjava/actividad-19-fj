
import com.cotizacionweb.cotizacion.Cotizacion;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotizacionTest {

    public CotizacionTest() {
    }

    @Test
    public void getFecha_ValoresValidosObjetoCotizacion_fechaObtenida() {
        Date resultadoEsperado = new Date();
        Cotizacion cotizacion = new Cotizacion("CoinDesk", resultadoEsperado, "USD", 6500.09);
        Date resultadoObtenido = cotizacion.getFecha();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void getMoneda_ValoresValidosObjetoCotizacion_monedaObtenida() {
        String resultadoEsperado = "USD";
        Cotizacion cotizacion = new Cotizacion("CoinDesk", new Date(), resultadoEsperado, 6500.09);
        String resultadoObtenido = cotizacion.getMoneda();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void getPrecio_ValoresValidosObjetoCotizacion_precioObtenido() {
        double resultadoEsperado = 100.09;
        Cotizacion cotizacion = new Cotizacion("CoinDesk", new Date(), "USD", resultadoEsperado);
        double resultadoObtenido = cotizacion.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido, 0.001);
    }

    @Test
    public void toString_ValoresValidosObjetoCotizacion_StringObtenido() {
        String fechaActual = new SimpleDateFormat("d-M-yyyy HH:mm").format(new Date());
        String resultadoEsperado = "CoinDesk:\t" + fechaActual + " / USD 100,01";
        Cotizacion cotizacion = new Cotizacion("CoinDesk", new Date(), "USD", 100.009f);
        String resultadoObtenido = cotizacion.toString();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
}
