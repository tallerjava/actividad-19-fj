
import com.cotizacionweb.repository.SomosPNTCotizacionRepository;
import jodd.http.HttpException;
import org.json.JSONException;
import org.junit.Test;

public class SomosPNTCotizacionRepositoryTest {

    public SomosPNTCotizacionRepositoryTest() {
    }

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_urlValidaServicioNoResponde_HttpException() {
        SomosPNTCotizacionRepository somosPNTCotizacionRepository = new SomosPNTCotizacionRepository();
        somosPNTCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_sinUrl_HttpException() {
        SomosPNTCotizacionRepository somosPNTCotizacionRepository = new SomosPNTCotizacionRepository();
        somosPNTCotizacionRepository.setUrl("");
        somosPNTCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = JSONException.class)
    public void obtenerCotizacion_urlInvalida_JSONException() {
        SomosPNTCotizacionRepository somosPNTCotizacionRepository = new SomosPNTCotizacionRepository();
        somosPNTCotizacionRepository.setUrl("https://api.coindesk.com/v1/bpi/currentprice.xml");
        somosPNTCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = NullPointerException.class)
    public void obtenerCotizacion_urlNull_NullPointerException() {
        SomosPNTCotizacionRepository somosPNTCotizacionRepository = new SomosPNTCotizacionRepository();
        somosPNTCotizacionRepository.setUrl(null);
        somosPNTCotizacionRepository.obtenerCotizacion();
    }
}
