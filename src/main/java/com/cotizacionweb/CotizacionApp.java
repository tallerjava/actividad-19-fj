package com.cotizacionweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CotizacionApp {

    public static void main(String[] args) {
        SpringApplication.run(CotizacionApp.class, args);
    }
}