package com.cotizacionWeb.cotizacion;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Cotizacion {

    private String nombreProveedor;
    private Date fecha;
    private String moneda;
    private double precio;

    public Cotizacion(String nombreProveedor, Date fecha, String moneda, double precio) {
        this.nombreProveedor = nombreProveedor;
        this.fecha = fecha;
        this.moneda = moneda;
        this.precio = precio;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public Date getFecha() {
        return fecha;
    }

    public String getMoneda() {
        return moneda;
    }

    public double getPrecio() {
        return precio;
    }

    @Override
    public String toString() {
        DecimalFormat formatoDecimal = new DecimalFormat(".##");
        String formatFecha = new SimpleDateFormat("d-M-yyyy HH:mm").format(fecha);
        return nombreProveedor + ":\t" + formatFecha + " / " + moneda + " " + formatoDecimal.format(precio);
    }
}
