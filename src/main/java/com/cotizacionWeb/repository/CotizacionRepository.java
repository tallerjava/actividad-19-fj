package com.cotizacionWeb.repository;

import com.cotizacionWeb.cotizacion.Cotizacion;
import org.springframework.stereotype.Component;

@Component
public abstract class CotizacionRepository {

    public abstract String getNombreProveedor();

    public abstract Cotizacion obtenerCotizacion();

    public abstract String getMoneda();
}
