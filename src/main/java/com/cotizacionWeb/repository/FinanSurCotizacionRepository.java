package com.cotizacionWeb.repository;

import com.cotizacionWeb.cotizacion.Cotizacion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import org.springframework.stereotype.Component;

@Component
public class FinanSurCotizacionRepository extends CotizacionRepository {

    private String urlLocal = "jdbc:mysql://localhost:3306/BaseLocal";
    private String nombreProveedor = "FinanSur";
    private String moneda = "USD";
    private String usuario = "usuario";
    private String password = "caperusita20";

    public void setUrlLocal(String urlLocal) {
        this.urlLocal = urlLocal;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUrlLocal() {
        return urlLocal;
    }

    public String getUsuario() {
        return usuario;
    }

    @Override
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    @Override
    public String getMoneda() {
        return moneda;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        Cotizacion cotizacion = null;
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            DriverManager.setLoginTimeout(3);
        } catch (Exception registerDriverManagerException) {
            registerDriverManagerException.printStackTrace();
        }
        try (Connection conexion = DriverManager.getConnection(urlLocal, usuario, password); Statement estado = conexion.createStatement();) {
            ResultSet resultado = estado.executeQuery("select * from cotizacion_historico where proveedor = 'FinanSur' and fecha <= NOW() order by fecha desc limit 0,1;");
            while (resultado.next()) {
                Date fechaDate = resultado.getTimestamp("fecha");
                String moneda = resultado.getString("moneda");
                double precio = resultado.getDouble("precio");
                cotizacion = new Cotizacion(nombreProveedor, fechaDate, moneda, precio);
            }
            estado.close();
            conexion.close();
        } catch (SQLException obtenerCotizacionException) {
            obtenerCotizacionException.printStackTrace();
        }
        if (cotizacion == null) {
            throw new NullPointerException("No se ha podido realizar la operacion. Se generó una cotizacion nula.");
        } else {
            return cotizacion;
        }
    }
}
