package com.cotizacionWeb.repository;

import com.cotizacionWeb.cotizacion.Cotizacion;
import java.util.Date;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class BinanceCotizacionRepository extends CotizacionRepository {

    private String url = "https://api.binance.com/api/v1/ticker/price?symbol=BTCUSDT";
    private String nombreProveedor = "Binance";
    private String moneda = "USD";

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    @Override
    public String getMoneda() {
        return moneda;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        HttpResponse response = HttpRequest.get(url).send();
        JSONObject body = new JSONObject(response.body());
        Date fecha = new Date();
        double precio = body.getDouble("price");
        Cotizacion cotizacion = new Cotizacion(nombreProveedor, fecha, moneda, precio);
        return cotizacion;
    }
}
