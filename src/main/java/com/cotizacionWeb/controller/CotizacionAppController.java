package com.cotizacionWeb.presentacion.controller;

import com.cotizacionWeb.cotizacion.Cotizacion;
import com.cotizacionWeb.service.CotizacionService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CotizacionAppController {
    
    @Autowired
    private CotizacionService cotizacionService;

    @RequestMapping(value = "/ListadoCotizaciones")
    public String ListadoCotizaciones(Model modelo) {
        ArrayList<Cotizacion> cotizacionListado = cotizacionService.obtenerListadoCotizacion();
        modelo.addAttribute("cotizacionListado", cotizacionListado);
        return "ListadoCotizaciones";
    }
}
